using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{

    [SerializeField] GameObject boss;
    [SerializeField] GameObject barrier;
    // Start is called before the first frame update
    void Start()
    {
        barrier.SetActive(false);
        boss.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            boss.SetActive(true);
            barrier.SetActive(true);
        }
    }
}
