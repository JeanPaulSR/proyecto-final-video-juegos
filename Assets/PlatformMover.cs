using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour
{
    [SerializeField] private float movementDistance;
    [SerializeField] private float speed;
    [SerializeField] private bool movingLeft;
    [SerializeField] private bool movingUp;
    private float leftEdge;
    private float rightEdge;
    private float upperEdge;
    private float lowerEdge;
    
    // Start is called before the first frame update
    void Start()
    {
        leftEdge = transform.position.x - movementDistance;
        rightEdge = transform.position.x + movementDistance;

        upperEdge = transform.position.y + movementDistance;
        lowerEdge = transform.position.y - movementDistance;
    }

    // Update is called once per frame
    void Update()
    {
        if (!movingUp)
        {
            if (movingLeft)
            {
                if (transform.position.x > leftEdge)
                {
                    transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
                }
                else
                    movingLeft = false;
            }
            else
            {

                if (transform.position.x < rightEdge)
                {
                    transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
                }
                else
                    movingLeft = true;
            }
        }
        else
        {

            if (movingLeft)
            {
                if (transform.position.y > lowerEdge)
                {
                    transform.position = new Vector3(transform.position.x , transform.position.y - speed * Time.deltaTime, transform.position.z);
                }
                else
                    movingLeft = false;
            }
            else
            {

                if (transform.position.y < upperEdge)
                {
                    transform.position = new Vector3(transform.position.x , transform.position.y + speed * Time.deltaTime, transform.position.z);
                }
                else
                    movingLeft = true;
            }
        }
    }
    
}
