using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StormProjectile : MonoBehaviour
{
    [SerializeField] float speed;
    private float lifetime;

    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = -transform.up * speed;
        lifetime = 0;
        lifetime += Time.deltaTime;
        if (lifetime > 3)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "Wall" || collision.tag == "Ground")
        {
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
