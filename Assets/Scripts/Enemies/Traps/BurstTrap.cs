using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstTrap : EnemyDamage
{
    [Header ("BurstTrap Attributes")]
    [SerializeField] private float speed;
    [SerializeField] private float range;
    [SerializeField] private float checkDelay;
    [SerializeField] private LayerMask playerLayer;
    private float checkTimer;
    private Vector3 destination;

    private bool attacking;
    private bool primed;

    private Vector3[] directions = new Vector3[4];

    private void OnEnable()
    {
        CalculateDirections();
        Stop();
        primed = true;
    }
    private void Update()
    {
        for (int i = 0; i < directions.Length; i++)
        {
            Debug.DrawRay(transform.position, directions[i], Color.red);
        }
        //Move trap towards destination only if attacking
        if (attacking && primed)
            transform.Translate(destination * Time.deltaTime * speed);
        else
        {
            checkTimer += Time.deltaTime;
            if (checkTimer > checkDelay)
                CheckForPlayer();
        }
    }

    private void CheckForPlayer()
    {

        for (int i = 0; i < directions.Length; i++)
        {
            Debug.DrawRay(transform.position, directions[i], Color.red);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, directions[i], range, playerLayer);

            if (hit.collider != null && !attacking)
            {
                attacking = true;
                destination = directions[i];
                checkTimer = 0;
            }
        }
    }

    private void CalculateDirections()
    {
        directions[0] = transform.right * range; //Right direction
        directions[1] = -transform.right * range; //Left direction
        directions[2] = transform.up * range; //Up direction
        directions[3] = -transform.up * range; //Down direction
    }

    //Stops the trap in it's place
    private void Stop()
    {
        destination = transform.position;
        attacking = false;

    }

    private new void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        //Stop once it hits something
        Stop();
        primed = false;
        gameObject.SetActive(false);
    }
}
