using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedTrap : MonoBehaviour
{
    [Header("Damage")]
    [SerializeField] private float damage;

    [Header("Firetrap Timers")]
    [SerializeField] private float activiationDelay;
    [SerializeField] private float activeTime;
    private Animator anim;
    private SpriteRenderer spriteRend;

    private bool triggered;
    private bool active;

    private Health player;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        spriteRend = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (!triggered)
            {
                StartCoroutine(ActivateTrap());
            }
            player = collision.GetComponent<Health>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        player = null;
    }

    private void Update()
    {
        if (active && player != null)
            player.TakeDamge(damage);
    }

    private IEnumerator ActivateTrap()
    {
        //Notify player trap is triggered
        triggered = true;
        spriteRend.color = Color.blue; //Warns player trap was triggered

        //Give a delay, then activate trap, then turn color back to know
        yield return new WaitForSeconds(activiationDelay);
        spriteRend.color = Color.white; //Warns player trap was triggered
        active = true;
        anim.SetBool("activated", true);
        
            //Wait x seconds, then deactivate trap and reset variables
        yield return new WaitForSeconds(activeTime);
        active = false;
        triggered = false;
        anim.SetBool("activated", false);
    }
}
