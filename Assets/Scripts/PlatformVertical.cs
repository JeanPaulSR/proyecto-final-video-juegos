using UnityEngine;

public class PlatformVertical : MonoBehaviour
{
    [Header("Patrol Points")]
    [SerializeField] private Transform bottomEdge;
    [SerializeField] private Transform topEdge;

    [Header("Platform")]
    [SerializeField] private Transform platform;

    [Header("Movement parameters")]
    [SerializeField] private float speed;
    private Vector3 initScale;
    private bool movingDown;

    [Header("Idle behavior")]
    [SerializeField] private float idleDuration;
    private float idleTimer;

    private void Awake()
    {
        initScale = platform.localScale;
    }

    private void Update()
    {
        if (movingDown)
        {
            if (platform.position.y >= bottomEdge.position.y)
                MoveInDirection(-1);
            else
                DirectionChange();
        }
        else
        {
            if (platform.position.y <= topEdge.position.y)
                MoveInDirection(1);
            else
                DirectionChange();
        }
    }

    private void DirectionChange()
    {
        idleTimer += Time.deltaTime;

        if (idleTimer > idleDuration)
            movingDown = !movingDown;
    }

    private void MoveInDirection(int _direction)
    {
        idleTimer = 0;

        //Make platform face direction
        platform.localScale = new Vector3(initScale.x,
            Mathf.Abs(initScale.y) * _direction, initScale.z);

        //Move in that direction
        platform.position = new Vector3(platform.position.x,
            platform.position.y + Time.deltaTime * _direction * speed, platform.position.z);
    }
}