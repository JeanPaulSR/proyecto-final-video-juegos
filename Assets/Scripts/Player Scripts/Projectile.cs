using UnityEngine;

public class Projectile : MonoBehaviour
{
    //Projectile speed
    [SerializeField] private float speed;
    [SerializeField] private float damage;
    //Checks if the projectile has hit something
    private bool hit;
    //Direction of the projectile
    private float direction;

    //BoxCollider2D reference
    private BoxCollider2D bc;
    //Animator reference
    private Animator anim;

    private float lifetime;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        bc = GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        if (hit)
            return;
        float movementSpeed = speed * Time.deltaTime * direction;
        transform.Translate(movementSpeed, 0, 0);

        lifetime += Time.deltaTime;
        if (lifetime > 1.5)
            gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Enemy" || collision.tag == "Wall" || collision.tag == "Ground")
        {
            hit = true;
            bc.enabled = false;
            anim.SetTrigger("explode");
            if(collision.tag == "Enemy")
                collision.GetComponent<Health>().TakeDamge(damage);
        }
     }

    public void SetDirection(float _direction)
    {
        lifetime = 0;
        direction = _direction;
        gameObject.SetActive(true);
        hit = false;
        bc.enabled = true;
        float localScaleX = transform.localScale.x;
        if (Mathf.Sign(localScaleX) != _direction)
            localScaleX = -localScaleX;
        
        transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
