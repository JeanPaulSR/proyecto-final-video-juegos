
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    //RigidBody2D reference
    private Rigidbody2D rb;
    //Modifiable velocity value
    [SerializeField] private float velocity;
    //Modifiable jump value
    [SerializeField] private float jump;
    //BoxCollider2D reference
    private BoxCollider2D bc;
    //Horizontal Input
    private float horizontalInput;
    //GroundLayer reference
    [SerializeField] private LayerMask groundLayer;
    //WallLayer reference
    [SerializeField] private LayerMask wallLayer;

    public Vector3 startPosition;
    private int respawnHealth;
    private int respawnShield;

    //
    [SerializeField] PlayerRespawn respawn;
    public bool checkpoint;
    //

    private GameObject[] players;
    private void Start()
    {
        //DontDestroyOnLoad(gameObject);
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 1)
        {
            Destroy(players[1]);
        }
        FindStartPos();
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    private void OnLevelWasLoaded(int level)
    {
        FindStartPos();
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 1)
        {
            for (int i = 1; i < players.Length; i++)
            {
                Destroy(players[i]);
            }
        }
    }
        

    void FindStartPos()
    {
        if (GameObject.FindWithTag("StartPos") != null)
            transform.position = GameObject.FindWithTag("StartPos").transform.position;
        if(checkpoint)
            transform.position = startPosition;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);

    }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        FindStartPos();
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players != null)
        {
            if (players.Length > 1)
            {
                Destroy(players[1]);
            }
        }
        //Defining rigid body reference
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        if (!Pause.isPaused)
        {
            //Horizontal input
            horizontalInput = Input.GetAxis("Horizontal");

            //Movement
            rb.velocity = new Vector2(horizontalInput * velocity, rb.velocity.y);

            //If moving right, flip right
            if (horizontalInput > 0.01f)
                transform.localScale = new Vector3(1, 1, 1);
            //If moving left, flip left
            if (horizontalInput < -0.01f)
                transform.localScale = new Vector3(-1, 1, 1);

            //Jumping
            if (Input.GetKey(KeyCode.Space) && isGrounded())
                Jump();
        }
    }

    //Jump method
    private void Jump()
    {
        if (!isGrounded() || onWall())
            return;
        if(isGrounded())
            rb.velocity = new Vector2(rb.velocity.x, jump);
    }

    //Checks if player is on the ground
    private bool isGrounded()
    {
        RaycastHit2D rayCastHit = Physics2D.BoxCast(bc.bounds.center, bc.bounds.size, 0, Vector2.down, 0.1f, groundLayer);
        return rayCastHit.collider != null;
    }

    //Checks if player is on a wall
    private bool onWall()
    {
        RaycastHit2D rayCastHit = Physics2D.BoxCast(bc.bounds.center, bc.bounds.size, 0, new Vector2(transform.localScale.x, 0), 0.1f, wallLayer);
        return rayCastHit.collider != null;
    }


    public bool canAttack()
    {
        return true;//isGrounded();
    }
}
