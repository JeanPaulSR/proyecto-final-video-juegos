using UnityEngine;

public class ShieldWrench : MonoBehaviour
{
    [SerializeField] private float shieldValue;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Health>().IncreaseShield(shieldValue);
            gameObject.SetActive(false);
        }
    }
}
