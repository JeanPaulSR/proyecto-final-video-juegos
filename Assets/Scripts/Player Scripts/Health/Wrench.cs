using UnityEngine;

public class Wrench : MonoBehaviour
{
    [SerializeField] private float repairValue;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Health>().Repair(repairValue);
            gameObject.SetActive(false);
        }
    }
}
