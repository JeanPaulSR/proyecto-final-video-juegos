using UnityEngine;

public class GoldenWrench : MonoBehaviour
{
    [SerializeField] private float healthIncreaseValue;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<Health>().Increase(healthIncreaseValue);
            gameObject.SetActive(false);
        }
    }
}
