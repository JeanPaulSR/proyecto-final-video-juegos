using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    [Header ("Health")]
    //Initial starting health
    [SerializeField] private float startingHealth;
    //Current health, set made a private function
    public float currentHealth { get; private set; }
    public float shield;
    //If the player is dead
    private bool dead;

    [Header("iFrames")]
    [SerializeField] public float iFramesDuration;
    [SerializeField] public int numberOfFlashes;
    private SpriteRenderer spriteRenderer;

    [Header("Components")]
    [SerializeField] private Behaviour[] components;
    private bool invulnerable;
    private Animator anim;

    private float checkpointHealth;
    private float checkpointMaxHealth;
    private float checkpointShield;

    private void Awake()
    {
        shield = 0;
        currentHealth = startingHealth;
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public void Respawn()
    {
        Repair(startingHealth);
        anim.ResetTrigger("die");
        anim.Play("Idle");
        dead = false;
        StartCoroutine(Invulnerability());

        foreach (Behaviour component in components)
            component.enabled = true;

        startingHealth = checkpointMaxHealth;
        currentHealth = checkpointHealth;
        shield = checkpointShield;
        //
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //
    }

    public void Checkpoint()
    {
        checkpointHealth = currentHealth;
        checkpointMaxHealth = startingHealth;
        checkpointShield = shield;
    }
    public void TakeDamge(float _damage)
    {
        if (invulnerable)
            return;

        float damageTaken = _damage;
        if(shield > 0)
        {
            if (shield > damageTaken)
            {
                shield = shield - damageTaken;
                damageTaken = 0;
            }
            else
            {
                shield = 0;
                damageTaken = Mathf.Abs(shield - damageTaken);
            }
        }

        currentHealth = Mathf.Clamp(currentHealth - damageTaken, 0, startingHealth);
        
        if(currentHealth > 0)
        {
            anim.SetTrigger("hurt");
            StartCoroutine(Invulnerability());
        }
        else
        {
            if (!dead)
            {
                anim.SetTrigger("die");
                //Deactivate Movement
                foreach (Behaviour component in components)
                    component.enabled = false;
                dead = true;
                if (gameObject.tag != "Player")
                    Destroy(gameObject);
            }
        }
    }

    public void Repair(float _value)
    {
        currentHealth = Mathf.Clamp(currentHealth + _value, 0, startingHealth);
    }

    public void Increase(float _value)
    {
        startingHealth = startingHealth + _value;
        currentHealth = startingHealth;
    }

    public void IncreaseShield(float _value)
    {
        shield = Mathf.Clamp(shield + _value, 0, startingHealth);
    }

    public float GetFull()
    {
        return startingHealth;
    }

    public void InvcinibilityPowerUp()
    {
        StartCoroutine(InvulnerabilityA());

    }
    public IEnumerator InvulnerabilityA()
    {
        invulnerable = true;
        Physics2D.IgnoreLayerCollision(9, 11, true);

        for (int i = 0; i < 5; i++)
        {
            spriteRenderer.color = new Color(0, 255, 0, 0.5f);
            yield return new WaitForSeconds(.25f);
            spriteRenderer.color = Color.white;
            yield return new WaitForSeconds(.25f);
        }

        Physics2D.IgnoreLayerCollision(9, 11, false);
        invulnerable = false;
    }
    public IEnumerator Invulnerability()
    {
        invulnerable = true;
        Physics2D.IgnoreLayerCollision(9,11, true);

        for (int i = 0; i < numberOfFlashes; i++)
        {
            spriteRenderer.color = new Color(1, 0, 0, 0.5f);
            yield return new WaitForSeconds(iFramesDuration / (numberOfFlashes * 2));
            spriteRenderer.color = Color.white;
            yield return new WaitForSeconds(iFramesDuration / (numberOfFlashes * 2));
        }

        Physics2D.IgnoreLayerCollision(9, 11, false);
        invulnerable = false;
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
