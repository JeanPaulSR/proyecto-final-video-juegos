using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Health playerHealth;
    [SerializeField] private Image healthBarTotal;
    [SerializeField] private Image healthBarCurrent;
    [SerializeField] private Image shieldBar;

    private void Start()
    {
        if (playerHealth == null)
            playerHealth = GameObject.Find("/Player").GetComponent<Health>();
        healthBarTotal.fillAmount = playerHealth.currentHealth / 10;
    }

    private void Update()
    {
        healthBarTotal.fillAmount = playerHealth.GetFull() / 10;
        healthBarCurrent.fillAmount = playerHealth.currentHealth / 10;
        shieldBar.fillAmount = playerHealth.shield / 10;
    }
}
