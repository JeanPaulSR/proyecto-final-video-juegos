using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ProjectileHolder : MonoBehaviour
{
    private GameObject[] projectileHolders;
    private void Awake()
    {
        projectileHolders = GameObject.FindGameObjectsWithTag("ProjectileHolder");

        if (projectileHolders.Length > 1)
        {
            Destroy(projectileHolders[1]);
        }
        DontDestroyOnLoad(gameObject);
    }
}
