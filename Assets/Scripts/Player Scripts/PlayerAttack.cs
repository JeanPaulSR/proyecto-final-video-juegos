using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private float attackCooldown;
    //Launch area for simple projectiles
    [SerializeField] private Transform spPoint;
    [SerializeField] private GameObject[] simpleProjectiles;
    private Animator anim;
    private PlayerMovement playerMovement;
    private float cooldownTimer = Mathf.Infinity;


    private void Awake()
    {
        anim = GetComponent<Animator>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        if (!Pause.isPaused)
        {
            if (Input.GetMouseButton(0) && cooldownTimer > attackCooldown && playerMovement.canAttack())
                Attack();

            cooldownTimer += Time.deltaTime;
        }
    }

    private void Attack()
    {
        //anim.SetTrigger("attack");
        cooldownTimer = 0;

        simpleProjectiles[FindProjectile()].transform.position = spPoint.position;
        simpleProjectiles[FindProjectile()].GetComponent<Projectile>().SetDirection(Mathf.Sign(transform.localScale.x));
    }

    private int FindProjectile()
    {
        for (int i = 0; i < simpleProjectiles.Length; i++)
        {
            if (!simpleProjectiles[i].activeInHierarchy)
                return i;
        }
        return 0;
    }
}
