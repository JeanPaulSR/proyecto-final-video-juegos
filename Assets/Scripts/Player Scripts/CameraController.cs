using UnityEngine;

public class CameraController : MonoBehaviour
{
    //Player reference
    [SerializeField] private Transform player;
    //Distance ahead of player for camera
    [SerializeField] private float aheadDistance;
    //Speed camera moves ahead of player 
    [SerializeField] private float cameraSpeed;
    private float lookAhead;

    private float storedSize;
    private float storedDistance;
    
    private float currentDistance;

    private bool changed;

    private void Start()
    {
        currentDistance = 1.5f;
        if (player == null)
            AssignPlayer();
    }
    private void Update()
    {
        transform.position = new Vector3(player.position.x + lookAhead, player.position.y + currentDistance, transform.position.z);
        lookAhead = Mathf.Lerp(lookAhead, (aheadDistance * player.localScale.x), Time.deltaTime * cameraSpeed /1.2f);
    }

    private void AssignPlayer()
    {
        player = GameObject.Find("/Player").transform;
    }

    public void ChangeCamera(float newSize)
    {
        if (!changed)
        {
            changed = true;
            Camera cam = gameObject.GetComponent<Camera>();
            storedSize = cam.orthographicSize;
            cam.orthographicSize = newSize;
            storedDistance = currentDistance;
            currentDistance = newSize / 2;
        }

    }

    public void RevertCamera()
    {
        if (changed)
        {
            changed = false;
            Camera cam = gameObject.GetComponent<Camera>();
            cam.orthographicSize = storedSize;
            currentDistance = storedDistance;
        }

    }
}
