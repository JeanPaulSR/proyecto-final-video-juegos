using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    private Transform currentCheckpoint;
    private Health playerHealth;
    //
    private PlayerMovement playerMovement;

    private void Awake()
    {
        playerHealth = GetComponent<Health>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    public void Respawn()
    {
        transform.position = currentCheckpoint.position;
        playerMovement.checkpoint = true;
        playerMovement.startPosition =  currentCheckpoint.position;
        playerHealth.Respawn();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Checkpoint")
        { 
            currentCheckpoint = collision.transform;
            collision.GetComponent<Collider2D>().enabled = false;
            collision.GetComponent<Animator>().SetTrigger("appear");

            playerHealth.Checkpoint();

        }
    }
}
