using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [SerializeField] GameObject storm;

    private bool behavior = false;
    private float timer = 0;
    private float wait = Mathf.Infinity;

    // Update is called once per frame
    void Update()
    {
        if (!behavior)
        {
            behavior = true;
            int decider = Random.Range(0, 2);
            
            switch(decider){
                case 1:
                    Hail();
                    wait = 3;
                    timer = 0;
                    break;
                default:
                    behavior = false;
                    break;

            }

        }
        else
        {
            timer += Time.deltaTime;
            if(timer > wait)
            {
                timer = 0;
                behavior = false;
            }
        }
    }

    void Hail()
    {
        storm.SetActive(true);
        storm.GetComponent<SummonStorm>().Summon();
    }
    
}
