using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject cam;

    [SerializeField] bool zoom;
    [SerializeField] float size;
    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("/Core/Main Camera");

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (zoom)
            {
                cam.GetComponent<CameraController>().ChangeCamera(size);
            }
            else
            {
                cam.GetComponent<CameraController>().RevertCamera();

            }
        }
    }
}
