using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonStorm : MonoBehaviour
{
    [SerializeField] GameObject projectile;

    [Header("Arena Edge")]
    [SerializeField] private Transform leftEdge;
    [SerializeField] private Transform rightEdge;

    [SerializeField] LineRenderer lineRenderer;
    private Transform playerTransform;
    private float playerX;
    private float playerY;
    private bool summoned;

    private float lifetime = 3;
    private float currentTime = 0;
    [SerializeField] float cooldown;
    private float timer;
    

    private void Awake()
    {
        summoned = false;
        timer = Mathf.Infinity;
    }


    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (summoned)
        {
            if (timer > cooldown)
            {
                timer = 0;
                Shoot();
            }
            else
                timer += Time.deltaTime;
        }
        if(currentTime > lifetime)
        {
            Desummon();
        }
        
    }

    public void Summon()
    {
        if (!summoned)
        {
            summoned = true;
            playerTransform = GameObject.Find("Player").transform;
            playerX = playerTransform.position.x;
            playerY = playerTransform.position.y;
            if (playerX - 3 <= leftEdge.position.x)
                playerX = leftEdge.position.x + 3;
            if (playerX + 3 >= rightEdge.position.x)
                playerX = rightEdge.position.x - 3;
            lineRenderer.SetPosition(0, new Vector3(playerX - 3, playerY + 5, 0));
            lineRenderer.SetPosition(1, new Vector3(playerX + 3, playerY + 5, 0));
            lineRenderer.enabled = true;
        }

    }

    void Desummon()
    {
        summoned = false;
        lineRenderer.enabled = false;
        currentTime = 0;
        timer = Mathf.Infinity;
        gameObject.SetActive(false);

    }

    public void Shoot()
    {
        float range = Random.Range(-3.0f, 3.0f);
        if (playerX - range <= leftEdge.position.x)
            range += 3;
        if (playerX + range >= rightEdge.position.x)
            range -= 3;
        Instantiate(projectile, new Vector3(playerX + range, playerY + 5, 0), Quaternion.identity);
    }
}
