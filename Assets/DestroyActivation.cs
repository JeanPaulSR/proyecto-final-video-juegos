using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyActivation : MonoBehaviour
{
    [SerializeField] GameObject destroyedObject;
    private SpriteRenderer sr;
    private LevelLoader ll;
    private BoxCollider2D bc;
    
    // Start is called before the first frame update
    void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
        ll = gameObject.GetComponent<LevelLoader>();
        bc = gameObject.GetComponent<BoxCollider2D>();
        sr.enabled = false;
        ll.enabled = false;
        bc.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (destroyedObject == null)
        {
            sr.enabled = true;
            ll.enabled = true;
            bc.enabled = true;
            print("Destroyed");
        }
        else
        {
            sr.enabled = false;
            ll.enabled = false;
            bc.enabled = false;

        }
    }
}
